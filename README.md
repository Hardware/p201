# ct2-dkms

This repository manages the construction of the ct2 driver for Debian 8 for now (April 2020). 

The continuous integration using a docker image was abandoned, as the docker image couldn't provide the header files for the host's kernel version.
Building it on a debian8 host by hand was possible and the file is avalable from this branch.

To create a new version of the package
--------------------------------------
- Do not forget to work on a separate branch and not in master
- Update dkms.conf file for the new version (dkms.conf file should be located inside the new directory)
- Update .gitlab-ci.yml file with the new version
- Commit and push to test, CI should prepare your package for you. You can download it as an artifact.

To manually build the package
-----------------------------
```
cp -r ct2-X.X /usr/src/ # Where X.X is the ct2 version.
dkms add ct2/X.X
dkms build ct2/X.X
dkms mkdsc ct2/X.X
dkms mkdeb ct2/X.X --source-only
```
Generated deb file is located in `/var/lib/dkms/ct2/X.X/deb`

To manually install the module
------------------------------
```
cp -r ct2-X.X /usr/src/ # Where X.X is the ct2 version.
dkms add ct2/X.X
dkms build ct2/X.X
dkms install ct2/X.X
```
